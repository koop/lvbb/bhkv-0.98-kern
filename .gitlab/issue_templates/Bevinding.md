# Omschrijving
Geef hier een beknopte omschrijving van de bevinding.

# Gevolg
Geef aan wat er niet goed gaat als de bevinding niet opgepakt wordt.

# Betrokken partijen
Geef aan wie er last van heeft als de bevinding niet opgepakt wordt.

# Voorstel
Optioneel: geef een suggestie wat er in de standaard moet veranderen om
de bevinding op te lossen.
