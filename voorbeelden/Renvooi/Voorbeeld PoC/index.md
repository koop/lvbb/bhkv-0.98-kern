# Wijziging van een fragment van de Omgevingsregeling

Dit renvooivoorbeeld gebruikt de RegelingVersies die zijn gebruikt voor de Proof of Concept renvooibepaling die in 2019Q3 gerealiseerd is. 

| Bestand                            | Toelichting                                                  |
| ---------------------------------- | ------------------------------------------------------------ |
| VersieA.xml (was-RegelingVersie)   | FRBRExpression: /akn/nl/act/mnre1034/2019/REG0001/nld@2019-06-01;1-0. Bevat een fragment van een conceptversie van de omgevingsregeling met twee tabellen en twee illustraties. Ook zijn er een paar opmaakelementen en tekstonderdelen aan toegevoegd om variatie te beproeven in de renvooibepaling. |
| VersieB.xml (wordt-RegelingVersie) | FRBRExpression: /akn/nl/act/mnre1034/2019/REG0001/nld@2019-08-01;2-0. Deze RegelingVersie kent een aantal wijzigingen op de was-RegelingVersie. Deze wijzigingen zijn deels gebaseerd op een conceptversie van de invoerringsregeling en deels fictief (in de XML met comments aangegeven). Alle mutatieacties komen minimaal 1 keer langs evenals wijzigingen van illustraties en tabellen (1 met tekstuele wijzigingen en 1 met structuurwijzingen). |
| RegelingMutatie.xml                | Dit is de output van de Renvooiservice: een RegelingMutatie met hierbinnen alle wijzigingsbepalingen. |
| Plaatjes.zip                       | Voor de volledigheid zijn de plaatjes hier ook toegevoegd. NB de renvooiservice doet hier verder niets mee. |



