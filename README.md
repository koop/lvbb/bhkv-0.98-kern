# LVBB bronhouderkoppelvlak voor STOP

In deze Gitlab wordt de 0.98-kern-versie van het schema voor het LVBB-koppelvlak voor aanleveringen volgens STOP publiek bekend gemaakt.

Documentatie op <https://koop.gitlab.io/lvbb/bhkv-0.98-kern>

